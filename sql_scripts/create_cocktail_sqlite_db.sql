/*
 * Creates the DB to collect the CocktailDB data
 */
--PRAGMA foreign_keys=OFF;
--BEGIN TRANSACTION;

CREATE DATABASE cocktaildb;

CREATE TABLE cocktail_meta(
	id_drink INT NOT NULL,
	name_drink TEXT,
	successful_insert BOOL DEFAULT FALSE, 
    issue TEXT,
	n_ingredients INT DEFAULT -1,
	n_measures INT DEFAULT -1,
	date_modified TIMESTAMP,
	date_inserted TIMESTAMP
);

CREATE TABLE (
	id_drink INT PRIMARY KEY,
	name_drink TEXT,
	alt_name_drink TEXT,
	category TEXT,
	iba TEXT,
--Alcohol is not bool as optional is possible
alcoholic TEXT,
    ingredients TEXT,
	glass TEXT,
	instructions_ger TEXT
)
--PARTITION BY LIST(category)
;

CREATE TABLE cocktails_additional(
	id_drink INT PRIMARY KEY,
	tags TEXT,
	video TEXT,
	image TEXT,
    thumb TEXT
	image_attribution BOOL,
	creative_com_conf BOOL
);
--COMMIT;
