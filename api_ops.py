"""Functions to retrieve data from API"""
# TODO: Considere using async

import json
from types import SimpleNamespace
import http.client as client


def http(endpoint):
    try:
        conn = client.HTTPSConnection("www.thecocktaildb.com")
        conn.request("GET", f"/api/json/v1/1/{endpoint}")
        data = conn.getresponse().read().decode("UTF-8")
        conn.close()
        return data
    except:
        return None


def return_drinks_by_letter(letter):
    """
    Returns drinks from API as type ___,\
    by asking for every drink name starting with the letter.
    """
    drink_data = None
    try:
        response = http(f"search.php?f={letter}")
        if response is not None and len(response) != 0:
            data = json.loads(response, object_hook=lambda d: SimpleNamespace(**d))
            if data.drinks is not None:
                drink_data = data.drinks

    except Exception as e:
        print(f"Was not able to return data for letter: {letter}:\n{e}")

    return drink_data
