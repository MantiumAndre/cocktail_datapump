"""Functions to run the ETL operations."""

import datetime


from api_ops import return_drinks_by_letter
from db_ops import dict2db

from cocktail_settings import (
    str_cols,
    bool_cols,
    letters,
    cocktail_cols,
    additional_cols,
    meta_cols,
)


def transform_drink(drink):
    """Takes the drink namespace object, transforms it and returns as dict"""
    drink = vars(drink)
    ingreds = []
    for idx in range(1, 16):
        ing = drink[f"strIngredient{idx}"]
        am = drink[f"strMeasure{idx}"]
        if ing is not None:
            ing = ing.strip()
        if am is not None:
            am = am.strip().title()
            ingreds.append(f"{am} of {ing}")
    n_ingreds = len(ingreds)
    if n_ingreds > 0:
        ingreds = ", ".join(ingreds).replace("'", "´")
        ingreds = f"'{ingreds}'"
    else:
        ingreds = None

    new_drink = dict()
    new_drink["id_drink"] = int(drink["idDrink"])
    for old_key, new_key in str_cols.items():
        new_val = str(drink[old_key]).replace("'", "´")
        if new_val.lower() in ["null", "nan", "none"]:
            new_val = None
        new_drink[new_key] = f"'{new_val}'"
    new_drink["ingredients"] = ingreds
    for old_key, new_key in bool_cols.items():
        new_val = str(drink[old_key])
        if new_val.lower() in ["yes", "true", "alcoholic", "thecocktaildb.com"]:
            new_val = True
        elif new_val.lower() in ["no", "false", "none", "non alcoholic"]:
            new_val = False
        else:
            print(
                f"Strange key word in api data found to determine true or false:\n \
            For drink {drink}\n in key: {old_key},  found value: {drink[old_key]}"
            )
        new_drink[new_key] = new_val

    new_drink["n_ingredients"] = n_ingreds

    return new_drink


def return_drink_data(settings):
    """Generator that returns data from API requests, \
    containing the drink data."""
    print("About to return drinks by letter.")
    for let in letters:
        drinks_from_let = return_drinks_by_letter(let)
        if drinks_from_let is not None:
            # Reduce list length for dev:
            if settings["quick"]:
                print("Reducing amount of drink data, as the quick argument is used.")
                drinks_from_let = drinks_from_let[:5]

            for drink in drinks_from_let:
                yield drink


def write_drink_2_db(drink, ids_in_meta, settings, con):
    """Writes a drink data piece into the two tables and adds info to the meta data."""
    drink = transform_drink(drink)

    if not drink["id_drink"] in ids_in_meta:
        # Additional filter, as only Drinks with German data was requested
        if not drink["instructions_ger"].lower() == "none":
            #            print(f"Drink Instructions: {drink['instructions_ger']}")
            # Insert the main data to the db
            main_collection = [(col, drink[col]) for col in cocktail_cols]
            success_main, main_err_msg = dict2db(
                "cocktails", main_collection, settings, con
            )
            # Insert the additional, possibly non-important data
            add_collection = [(col, drink[col]) for col in additional_cols]
            success_add, add_err_msg = dict2db(
                "cocktails_additional", add_collection, settings, con
            )

            # Add the meta data
            success = success_main and success_add

            meta_collection = [(col, drink[col]) for col in meta_cols]
            meta_collection.append(("successful_insert", success))
            meta_collection.append(("issue", f"'{main_err_msg} - {add_err_msg}'"))
            meta_collection.append(("date_inserted", f"'{datetime.datetime.now()}'"))
            dict2db("cocktail_meta", meta_collection, settings, con)
