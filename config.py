"""Functions to return configuration settings"""

import os, sys
import argparse


def return_env():
    """Returns env vars from .env file as dict"""
    env_set = dict()

    try:
        # DB config
        env_set["db_type"] = os.environ["DB_TYPE"]
        env_set["db_name"] = os.environ.get("DB_NAME", "cocktail")
        # API config
        env_set["api_url"] = os.environ["API_URL"]
        env_set["api_key"] = os.environ.get("API_KEY", "1")

    except Exception as e:
        sys.exit(
            f"\n\nFATAL: Was not able to retrieve some env vars.\nCheck the Readme and update the .env file.\n{e}"
        )

    return env_set


def return_args():
    """Returns input arguments as dict"""
    parser = argparse.ArgumentParser(
        prog="cocktail_datapump.py",
        description="Takes data from CocktailDB and pumps it into a DB defined in the .env file.",
    )
    parser.add_argument(
        "-t",
        "--test",
        action="store_true",
        help="A switch to deactivate some parts of the code.",
        default=False,
    )  # Used for testing purposes, switch to deactivate something

    parser.add_argument(
        "-q",
        "--quick",
        action="store_true",
        help="A switch to reduce number of actions for quick tests.",
        default=False,
    )  # Used for testing purposes, switch to reduce amount of crunched data

    parser.add_argument(
        "-i",
        "--initialize",
        action="store_true",
        help="Argument to initialize the DB.",
        default=False,
    )  # Used for testing purposes, switch to reduce amount of crunched data

    parser.add_argument(
        "-e",
        "--etl",
        action="store_true",
        help="Run the ETL operations (API requests, write to DB).",
        default=True,
    )  # This runs the ETL steps (API requests, write to DB)

    input_args_dict = parser.parse_args()
    return input_args_dict


def get_settings():
    """Gets all settings from env, input arguments
    and returns them as unified dict.
    """
    env_sets = return_env()
    inp_args = return_args()

    settings = env_sets | vars(
        inp_args
    )  # Pipe is new in Python 3.9 to merge dictionaries

    return settings
