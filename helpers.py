"""Small helper functions for the Cocktail API Datapump"""

from cocktail_settings import vers
import sys, os


def print_art():
    # Prints cool info at start of software.

    print("\n██████████████████████████████████████")
    print("██░░░░░░██░░░░░░░░░░░░░░░░░░░░░░░░░░██")
    print("██      ██                        ██")
    print("  ██      ██                    ██")
    print("    ██      ░░░░░░            ██")
    print("      ██    ░░░░░░          ██")
    print("        ██  ░░░░██        ██")
    print("          ██      ██    ██")
    print("            ██        ██")
    print("              ██    ██")
    print("                ████")
    print("                ████")
    print("                ████")
    print("                ████")
    print("                ████")
    print("                ████")
    print("                ████")
    print("                ████                        Cocktail API Datapump")
    print(f"                ████                                  vers.: {vers}")
    print("                ████████")
    print("            ████████████                              by Cool Guy")
    print("        ▓▓▓▓████████████▓▓██                 coolguy@internet.com\n")


def read_sql_script(file_path):
    """Reads an SQL script and returns as str"""
    txt = ""
    if os.path.isfile(file_path):
        with open(file_path, "r") as file:
            txt = file.read().replace("\n", " ")
    else:
        sys.exit(
            "FATAL: Cannot find file: create_cocktail_sqlite_db.sql\n \
        it is needed to initialize the DB."
        )
    return txt
