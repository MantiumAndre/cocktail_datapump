"""Functions to interact with DB systems"""

import sys
import sqlite3
from helpers import read_sql_script


def get_db_connection(settings):
    """Returns connection object to interface with BD"""
    if settings["db_type"].lower() == "sqlite":
        con = sqlite3.connect(f"{settings['db_name']}.db")
        print(f"Got myself a db connection for {settings['db_type']}: {con}")
    else:
        sys.exit(
            "FATAL: Unknown db type defined in .env.\nTry sqlite (currently nothing else is implemented."
        )

    return con


def init_db(settings, con):
    """Initiazies DB by calling a script found in sql_scripts"""
    if settings["db_type"].lower() == "sqlite":
        init_file_path = "sql_scripts/create_cocktail_sqlite_db.sql"
        sys.exit(f"\nCURRENTLY THIS DOES NOT WORK AS THE DB DOES NOT GET CREATED!!!\n\n \
        Workaround for now as follows:\nType this:\n \
        cat {init_file_path} | sqlite3 {settings['db_name']}.db")
        # TODO: Figure out how to make the DB, before creating the tables and so on using the script!
        
        print(f"About to initialize DB,  by using: {init_file_path}")
        sql_qry = read_sql_script(init_file_path)

        try:
            cur = con.cursor()
            cur.executescript(sql_qry)
        except Exception as e:
            print(f"Was not able to initialize DB because:\n{e}")


def exec_sql_script(settings, con, sql_file):
    """Reads sql scripts, executes them and returns the result"""
    sql_qry = read_sql_script(sql_file)
    #    print(f"Executing SQL script:\n{sql_qry}")
    res = None
    try:
        cur = con.cursor()
        res = cur.execute(sql_qry).fetchall()
    except Exception as e:
        print(f"Was not able to execute sql script {sql_file},  because:\n{e}")

    return res


def dict2db(table_name, tuple_list, settings, con):
    """Takes a table name and a list of tuples and writes it to the DB.\
    Returns if operations was successful."""
    success = False
    msg = ""
    # Crazy workround, because sqlite seems to be too hard for me
    # TODO: Fix this, as it is causes multiple casting ops
    col_names_str = ", ".join([str(tup[0]) for tup in tuple_list])
    vals_str = ", ".join([str(tup[1]) for tup in tuple_list])
    insert_qry = f"insert into {table_name} ({col_names_str}) \
                    values ({vals_str});"

    try:
        cur = con.cursor()
        cur.execute(insert_qry)
        con.commit()
        success = True
        msg = f"Insert to {table_name} was successful"
    except Exception as e:
        msg = f"Was not able to write into table: {table_name},  because:\n{e}"
        print(msg)
        print(f"SQL query at fault:\n{insert_qry}")

    return success, msg


def close_db_cons(con):
    """Close DB con again."""
    print("Closing DB connection.")
    try:
        con.close()
    except Exception as e:
        print(f"Was not able to close DB connection because:\n{e}")
