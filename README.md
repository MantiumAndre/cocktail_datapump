# README #

The super cool CocktailAPI-Datapump

### Installation ###

This software is written in Python and uses [Pipenv](https://pipenv.pypa.io/en/latest/) for the virt. environment. Many environment variables are to be stored in and .env file, which the user needs to create.

#### Virt. Env ####
After installing Pipenv and cloning this repo, head to the respective folder and run:

    pipenv install


#### .env File ####
The .env file contains many settings and credentials used for this software:

    #DB CONFIG:
    export DB_TYPE='sqlite' #Define the db type (sqlite, psql)
    export DB_NAME='cocktaildb' #Name of db, without .db suffix

    #API config:
    export API_URL='www.thecocktaildb.com' #Url to API
    export API_KEY='' #API key (default is 1)


### Running the software ###
#### Get Help ####

    pipenv run python cocktail_datapump.py --help

#### First run ####
As the help has revealed, using the --initialize argument will initialize the DB

    pipenv run python cocktail_datapump.py --initialize

#### Normal Operation ####

    pipenv run python cocktail_datapump.py --etl
    
The etl argument is default, so it does not have to be specifically set.
