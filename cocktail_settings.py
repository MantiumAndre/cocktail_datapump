"""Various settings of this software can be defined here.
Please DO NOT ENTER ANY SECRETES HERE!!!"""

import string

vers = "v0.1"

letters = tuple(string.ascii_lowercase)
str_cols = {
    "strDrink": "name_drink",
    "strDrinkAlternate": "alt_name_drink",
    "strTags": "tags",
    "strVideo": "video",
    "strCategory": "category",
    "strIBA": "iba",
    "strGlass": "glass",
    "strInstructionsDE": "instructions_ger",
    "strDrinkThumb": "thumb",
    "strImageSource": "image",
    "strImageAttribution": "image_attribution",
    "strAlcoholic": "alcoholic",
    "dateModified": "date_modified",
}

bool_cols = {"strCreativeCommonsConfirmed": "creative_com_conf"}

# meta_cols = ('id_drink', 'name_drink', 'successful_insert', 'issue', 'date_modified', 'n_ingredients', 'date_inserted')
meta_cols = ("id_drink", "name_drink", "date_modified", "n_ingredients")
cocktail_cols = (
    "id_drink",
    "name_drink",
    "alt_name_drink",
    "category",
    "iba",
    "alcoholic",
    "ingredients",
    "glass",
    "instructions_ger",
)
additional_cols = (
    "id_drink",
    "tags",
    "video",
    "image",
    "thumb",
    "image_attribution",
    "creative_com_conf",
)
