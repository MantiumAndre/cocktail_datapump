"""Tool to downlaod data from CocktailDB and store it into DB of choice"""
# TODO: Add logging
# TODO: Use buld insert
# TODO: ADD PSQL support
# TODO: Deactivate autocommit and use rollback

import time
from helpers import print_art
from config import get_settings
from db_ops import get_db_connection, init_db, close_db_cons, exec_sql_script
from etl_ops import return_drink_data, write_drink_2_db



def main():

    print_art()
    print("Starting Cocktail API Datapump operations.\n")
    start_time = time.time()

    settings = get_settings()
    db_con = get_db_connection(settings)

    if settings["initialize"]:
        print("Setting up DB from scratch (make sure to delete it first).")
        init_db(settings, db_con)

    if settings["etl"]:

        drink_data = return_drink_data(settings)
        ids_in_meta = exec_sql_script(
            settings, db_con, "sql_scripts/return_ids_in_meta.sql"
        )
        ids_in_meta = [
            int(id[0]) for id in ids_in_meta
        ]  # Fix to convert the returned tuples to int
        for drink in drink_data:
            write_drink_2_db(drink, ids_in_meta, settings, db_con)

    close_db_cons(db_con)
    run_time = time.time() - start_time
    print(f"\nDone with all operations.\nTotal run time was {run_time:.2f}s.")


if __name__ == "__main__":
    main()
